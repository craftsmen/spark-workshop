package nl.craftsmen.spark.workshop.exercise1;

import nl.craftsmen.spark.workshop.util.FileUtils;
import nl.craftsmen.spark.workshop.util.WordUtil;
import org.apache.spark.sql.*;

import java.io.IOException;

/**
 * Exercise 1: Word count
 *
 * Read the textfile and count the occurences of every word. Write the output to a file.
 *
 * Then try the same with SQL.
 */
public class SparkWordCount {

    private static final String SHAKESPEARE_PATH = "/tmp/data/wordcount/shakespeare.txt";

    private static final String TMP_OUTPUT_PATH = "/tmp/data/wordcount/tmpWordcount.csv";

    private static final String OUTPUT_PATH = "/tmp/data/wordcount/wordcount.csv";

    private static final String SQL_QUERY = "SELECT value, COUNT(value) FROM Words GROUP BY value";

    public static void main(String[] args) throws IOException {

        // TODO: create a SparkSession and set the loglevel to WARN (so no spark logging is shown between output of this application)
        SparkSession sparkSession = null;

        // TODO: read the file shakespeare.txt to a dataset of Strings
        Dataset<String> dataset = null;
        dataset.show();

        // TODO: convert the dataset of lines into a dataset of words, using the WordUtil class to split a line into words
        Dataset<String> words = null;
        words.cache();

        // TODO: generate running word count
        Dataset<Row> wordCounts = null;
        wordCounts.show();

        // TODO: write the words including count back to a file when the count is greater than 100
        Dataset<Row> filteredWords = null;

        FileUtils.merge(TMP_OUTPUT_PATH, OUTPUT_PATH, true);

        // TODO: now try this with SQL by creating a table of the set of words and use the query to get the results.
        // first create a view named "Words"
        Dataset<Row> sqlResult = null;
        sqlResult.show(); // this sql result should be the same as the previous wordCounts result

        // close the session
        sparkSession.close();
    }
}
