package nl.craftsmen.spark.workshop.exercise2;

import edu.stanford.nlp.simple.Sentence;
import nl.craftsmen.spark.workshop.util.FileUtils;
import org.apache.spark.sql.*;
import scala.Tuple2;

import java.io.IOException;

public class SparkTwitterSentiment {

    private static final String TWEETS_PATH = "/tmp/data/tweets/tweets.json";

    private static final String OUTPUT_PATH = "/tmp/data/tweets/sentiments.json";

    private static final String TMP_OUTPUT_PATH = "/tmp/data/tweets/tmp.json";

    public static void main(String[] args) throws IOException {
        // TODO: create a SparkSession and set the loglevel to WARN (so no spark logging is shown between output of this application)
        SparkSession sparkSession = null;

        // TODO: load dataset (from TWEETS_PATH), which has a header at the first row
        Dataset<Row> twitterSet = null;

        // TODO: Create a dataset with only the tweets with #Spark
        Dataset<Row> sparkSet = null;

        // TODO: Determine the sentiment of the tweets containing the hashtag #spark using edu.stanford.nlp.simple.Sentence.sentiment()
        Dataset<Tuple2<String, String>> tupleSet = null;

        // TODO: merge the sentiment with the twitterSet and write to a new json file
        Dataset<Row> tweetSentimentSet = null;

        sparkSession.close();
    }
}
