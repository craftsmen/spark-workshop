package nl.craftsmen.spark.workshop.exercise4;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.ml.classification.RandomForestClassificationModel;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.tree.model.RandomForestModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

import java.io.IOException;
import java.util.*;
import java.util.function.DoubleConsumer;
import java.util.stream.DoubleStream;

/**
 * Exercise 4: Digit classification with machine learning
 * <p>
 * Read a file of number images and predict the number of the image by training a RandomForestClassificationModel model.
 * <p>
 * The input to train the model is a dataset of a label and a feature column with a vector of doubles.
 */
public class SparkDigitClassification {

    private static final String DIGITS_PATH = "/tmp/data/digits/digits.csv";

    private static final String MODEL_PATH = "/tmp/data/digits/myModel";

    public static void main(String[] args) throws IOException {

        // TODO: create a SparkSession and set the loglevel to WARN (so no spark logging is shown between output of this application)
        SparkSession sparkSession = null;

        // TODO: read the text file with the digits. First value is the number, followed by greyscale pixeldata
        Dataset<String> dataset = null;

        // TODO: transform the dataset to a set of labels and features
        // (which is a workaround for reading a csv with this schema, which does not work with a vector)
        StructType schema = null;
        Dataset<Row> labelAndFeatures = sparkSession.createDataFrame(getRdd(dataset), schema);

        // TODO: split dataset in training and test set, use seed for repetition
        long seed = 5043;
        Dataset<Row> trainingSet = null;
        Dataset<Row> testSet = null;

        // TODO: cache the sets, so they won't be calculated again

        // TODO: train the model using a RandomForestClassifier algorithm
        RandomForestClassificationModel model = null;

        // TODO: predict the outcome of the testset
        Dataset<Row> predictions = null;

        // TODO: evaluate the outcome and print the accuracy
        MulticlassClassificationEvaluator evaluator = null;

        System.out.println("accuracy: " + "???");

        // TODO: write the model to disk, if you want to use it later

        // TODO: try the prediction again with the saved model on the entire dataset

        sparkSession.close();
    }

    private static JavaRDD<Row> getRdd(Dataset<String> dataset) {
        return dataset.toJavaRDD().map(line -> {
            Tuple2<Double, Vector> tuple = splitLineIntoLabelAndFeatures(line);
            return RowFactory.create(tuple._1(), tuple._2());
        });
    }

    private static Tuple2<Double, Vector> splitLineIntoLabelAndFeatures(String line) {
        DoubleStream parts = Arrays.stream(line.split(",")).mapToDouble(Double::parseDouble);
        PrimitiveIterator.OfDouble iterator = parts.iterator();
        double label = iterator.next();
        List<Double> pixels = new ArrayList<>();
        iterator.forEachRemaining((DoubleConsumer) pixels::add);
        return new Tuple2<>(label, org.apache.spark.ml.linalg.Vectors.dense(pixels.stream().mapToDouble(Double::doubleValue).toArray()));
    }
}
