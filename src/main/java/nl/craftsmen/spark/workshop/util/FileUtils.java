package nl.craftsmen.spark.workshop.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

/**
 * Helper class to merge all files in a directory into a single file.
 */
public class FileUtils {

    public static void merge(String srcPath, String destPath, boolean overwrite) throws IOException {
        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(configuration);
        if (overwrite) {
            fileSystem.delete(new Path(destPath), false);
        }
        FileUtil.copyMerge(fileSystem, new Path(srcPath), fileSystem, new Path(destPath), true, configuration, null);
    }
}
