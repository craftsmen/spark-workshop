package nl.craftsmen.spark.workshop.exercise3;

import edu.stanford.nlp.simple.Sentence;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import scala.Tuple2;
import twitter4j.HashtagEntity;
import twitter4j.Status;

/**
 * Exercise 3: Determine the sentiment of a stream of tweets.
 *
 * Read a stream of tweets (1 batch of 1 second) and determine the sentiment of the tweets containing the hashtag #spark.
 * Print the text of the tweets with the sentiment to stdout.
 *
 * IF no tweets with hashtag #spark are found, determine the trending hashtags and do it again
 */
public class SparkTwitterStream {

    public static void main(String[] args) throws InterruptedException {
        // TODO: create a Streaming Spark Context for 2 second batches
        JavaStreamingContext streamingContext = null;
        streamingContext.sparkContext().setLogLevel("ERROR");

        // TODO: create a TwitterStream
        JavaDStream<Status> stream = null;

        System.out.println("Initializing Twitter stream...");

        // TODO: determine the sentiments of the tweets containing the hashtag #java or #spark
        // and print the text and sentiment to stdout
        JavaDStream<Tuple2<String, String>> sentimentStream = null;

        // LET OP: output verschijnt in stdout van workers (als je een cluster gebruikt anders in de stdout van je terminal),
        // te benaderen via de UI!!
        // Eventueel kun je ze wegschrijven naar files (hoe voorkom je dat de files dezelfde naam krijgen?)

        // Output kan leeg zijn als de hashtags niet in de stream voorkomen.
        // Bepaal dan trending hashtags door een stream van hashtags (zie status.getHashtagEntities())
        // te maken en op deze stream een countByValue te doen en de hashtag en count te sorteren en
        // naar stdout te printen
        //
        // en probeer het bepalen van het sentiment daarna nog eens met de nieuwe hashtags

        // TODO: determine what is trending on twitter right now
        // JavaDStream<HashtagEntity> hashtags = null;

        // TODO: start the proces, it won't work if you don't add this!
        System.out.println("Starting stream...");
    }

}
